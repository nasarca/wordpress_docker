# wordpress_docker

Proyecto Base Wordpress implementando Docker para soporte de PHP, Apache y MySQL

### Prerequisitos

* Instalar Docker
```https://store.docker.com/search?type=edition&offering=community```
* Tener una cuenta en Docker Hub
```https://hub.docker.com/```
* Chequear que en los puertos 8000 y 3306 no este corriendo ningún servicio, en ocasiones cuando tenemos MAMP activo estos puertos están en uso, en este caso detener mamp
* En la raiz del proyecto ejecutar el comando ```docker-compose up```
